#pragma once
#include "Vector3.h"

namespace flame
{
	namespace rt
	{
		namespace lights
		{
			struct DirectionLight
			{
				math::Vector3 direction;
				math::Vector3 colour;

				DirectionLight()
				{
					direction = math::Vector3(0.f, 0.f, 1.f);
					colour = math::Vector3(1.f, 1.f, 1.f);
				}

				DirectionLight(const math::Vector3 &direction, const math::Vector3 & colour)
				{
					this->direction = direction;
					this->colour = colour;
				}
			};
		}
	}
}