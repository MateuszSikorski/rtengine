#pragma once
#include "Vector3.h"

namespace flame
{
	namespace math
	{
		class Ray
		{
		public:
			Ray(const Vector3 &startPoint, const Vector3 &direction, float length) { CalculateData(startPoint, direction, length); }

			void SetStartPoint(const Vector3 &startPoint) { CalculateData(startPoint, direction, length); }
			Vector3 GetStartPoint() const { return startPoint; }

			Vector3 GetEndPoint() const { return endPoint; }

			void SetDirection(const Vector3 &direction) { CalculateData(startPoint, direction.GetNormalize(), length); }
			Vector3 GetDirection() const { return direction; }

			void SetLength(float length) { CalculateData(startPoint, direction, length); }
			float GetLength() const { return length; }

		private:
			Vector3 startPoint;
			Vector3 endPoint;
			Vector3 direction;
			float length;

			void CalculateData(Vector3 startPoint, Vector3 direction, float length)
			{
				this->startPoint = startPoint;
				this->direction = direction;
				this->length = length;

				endPoint = startPoint + direction * length;
			}
		};
	}
}