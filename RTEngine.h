#pragma once
#include <vector>
#include "Sphere.h"
#include "DirectionLight.h"
#include "Material.h"

namespace flame
{
	namespace rt
	{
		class RTEngine
		{
		public:
			RTEngine();
			virtual ~RTEngine();

			void InitColorBuffer(int bufferWidth, int bufferHeight);
			void SetEye(const math::Vector3 &position);
			void ClearColorBuffer();
			void Render();

			float *GetColorBuffer() { return &colorBuffer[0]; }

			int GetColorBufferWidth() { return colorBufferWidth; }
			int GetColorBufferHeight() { return colorBufferHeight; }

		private:
			std::vector<float> colorBuffer;
			int colorBufferWidth;
			int colorBufferHeight;

			math::Vector3 eyePosition;
		};
	}
}