#pragma once
#include "Ray.h"
#include "HitData.h"

namespace flame
{
	namespace math
	{
		struct Sphere
		{
		public:
			Vector3 center;
			float radius;

			Sphere()
			{
				center = Vector3(0,0,0);
				radius = 1;
			}

			Sphere(float x, float y, float z, float radius)
			{
				center.x = x;
				center.y = y;
				center.z = z;
				this->radius = radius;
			}

			Sphere(const Vector3 &center, float radius)
			{
				this->center = center;
				this->radius = radius;
			}

			HitData IsHited(const Ray &ray) const
			{
				auto rayStart = ray.GetStartPoint();
				auto rayEnd = ray.GetEndPoint();

				auto div = rayStart - rayEnd;
				auto u = Dot(rayEnd - rayStart, center - rayStart) / Dot(div, div);
				auto point = rayStart + (rayEnd - rayStart) * u;
				auto length = (point - center).LengthPow2();
				auto radiusPow2 = radius * radius;

				if (length > radiusPow2)
					return HitData(false);

				auto rayDirection = ray.GetDirection();
				auto z = std::sqrtf(radiusPow2 - length);
				auto hitPoint1 = point - rayDirection * z;
				auto hitPoint2 = point + rayDirection * z;
				auto normalToHitPoint1 = (hitPoint1 - rayStart).GetNormalize();
				auto normalToHitPoint2 = (hitPoint2 - rayStart).GetNormalize();

				auto culling1 = Dot(normalToHitPoint1, rayDirection) <= 0.f;
				auto culling2 = Dot(normalToHitPoint2, rayDirection) <= 0.f;

				if(culling1 && culling2)
					return HitData(false);

				if(culling1)
					return HitData(true, hitPoint2, (hitPoint2 - center).GetNormalize());
				if(culling2)
					return HitData(true, hitPoint1, (hitPoint1 - center).GetNormalize());

				if ((rayStart - hitPoint1).Length() < (rayStart - hitPoint2).Length())
					hitPoint2 = hitPoint1;

				return HitData(true, hitPoint2, (hitPoint2 - center).GetNormalize());
			}
		};
	}
}