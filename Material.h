#pragma once
#include "Vector3.h"

namespace flame
{
	namespace rt
	{
		namespace materials
		{
			struct Material
			{
				math::Vector3 colour;
				float reflection;

				Material()
				{
					colour = math::Vector3(1.f, 1.f, 1.f);
				}

				Material(const math::Vector3 &colour, float reflection)
				{
					this->colour = colour;
					this->reflection = reflection;
				}
			};
		}
	}
}