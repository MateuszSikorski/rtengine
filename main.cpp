#include <iostream>
#include "Vector3.h"
#include <GLFW/glfw3.h>
#include "RTEngine.h"

#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600

int main(int argc, char *argv[])
{
	GLFWwindow *window;
	if (!glfwInit())
		return -1;

	window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "RT Engine", nullptr, nullptr);
	if (!window)
	{
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);

	glEnable(GL_TEXTURE_2D);

	GLuint texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	flame::rt::RTEngine rtEngine;
	rtEngine.InitColorBuffer(WINDOW_WIDTH, WINDOW_HEIGHT);

	while (!glfwWindowShouldClose(window))
	{
		auto startTime = glfwGetTime();
		rtEngine.ClearColorBuffer();
		rtEngine.Render();
		auto endTime = glfwGetTime();
		std::cout << std::to_string(endTime - startTime) << "\n";


		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, rtEngine.GetColorBufferWidth(), rtEngine.GetColorBufferHeight(), 0, GL_RGB, GL_FLOAT, rtEngine.GetColorBuffer());


		glClearColor(1, 0, 0, 1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

		glLoadIdentity();
		glOrtho(0, WINDOW_WIDTH, 0, WINDOW_HEIGHT, -1, 1);

		glBegin(GL_QUADS);
		glTexCoord2f(0, 0);
		glVertex3f(0, 0, 0.f);
		glTexCoord2f(1, 0);
		glVertex3f(WINDOW_WIDTH, 0, 0.f);
		glTexCoord2f(1, 1);
		glVertex3f(WINDOW_WIDTH, WINDOW_HEIGHT, 0.f);
		glTexCoord2f(0, 1);
		glVertex3f(0, WINDOW_HEIGHT, 0.f);
		glEnd();

		glfwSwapBuffers(window);

		glfwPollEvents();
	}

	glDeleteTextures(1, &texture);

	glfwTerminate();

	return 0;
}