#include "RTEngine.h"

namespace flame
{
	namespace rt
	{
		RTEngine::RTEngine()
		{
			SetEye(math::Vector3(0.f, 0.f, 0.f));
		}

		RTEngine::~RTEngine()
		{
		}

		void RTEngine::InitColorBuffer(int bufferWidth, int bufferHeight)
		{
			colorBufferHeight = bufferHeight;
			colorBufferWidth = bufferWidth;

			colorBuffer.resize(bufferWidth * bufferHeight * 3);
		}

		void RTEngine::SetEye(const math::Vector3 &position)
		{
			eyePosition = position;
		}

		void RTEngine::ClearColorBuffer()
		{
			for (int i = 0; i < colorBufferWidth * colorBufferHeight * 3; i++)
				colorBuffer[i] = 0;
		}

		void RTEngine::Render()
		{
			std::vector<math::Sphere> spheres;
			spheres.push_back(math::Sphere(math::Vector3(-4, 0.5f, 5.5f), 0.5f));
			spheres.push_back(math::Sphere(math::Vector3(0, 0, 6), 1.f));
			spheres.push_back(math::Sphere(math::Vector3(4, 0, 6), 1.5f));

			materials::Material mat(math::Vector3(1.f, 0.f, 0.f), 0.5f);

			lights::DirectionLight directionLight(math::Vector3(1.f, 0.f, 0.f).GetNormalize(), math::Vector3(1, 1, 1));

			auto width = (float)colorBufferWidth;
			auto height = (float)colorBufferHeight;

			auto xValue = width / height;

			for (int i = 0; i < height; i++)
			{
				auto y = (float)i / height;
				auto vecY = (1.f - y) * -1.f + 1.f * y;

				for (int j = 0; j < width; j++)
				{
					auto x = (float)j / width;
					auto vecX = (1.f - x) * -xValue + xValue * x;

					math::Ray ray(eyePosition, math::Vector3(vecX, vecY, 1), 100);

					for (int k = 0; k < spheres.size(); k++)
					{
						auto hitData = spheres[k].IsHited(ray);
						if (hitData.IsHitted())
						{
							auto inShadow = false;
							for (int l = 0; l < spheres.size(); l++)
							{
								if (l == k)
									continue;

								auto dir = directionLight.direction * -1.f;
								math::Ray shadowRay(hitData.HitPoint(), dir, 100.f);
								auto shadowHitData = spheres[l].IsHited(shadowRay);
								if (shadowHitData.IsHitted())
								{
									inShadow = true;
									break;
								}
							}
							
							if (inShadow)
								continue;

							auto dir = directionLight.direction * -1.f;
							auto diffuseFactor = math::Dot(hitData.Normal(), dir);
							auto diffuseLight = directionLight.colour * diffuseFactor;

							auto reflect = math::Reflect(hitData.Normal(), directionLight.direction);
							auto specularFactor = std::powf(std::fmaxf(math::Dot(reflect, ray.GetDirection() * -1.f), 0.f), 8) * mat.reflection;

							auto index = (i * colorBufferWidth + j) * 3;
							colorBuffer[index] = mat.colour.r * diffuseLight.r + directionLight.colour.r * specularFactor;
							colorBuffer[index + 1] = mat.colour.g * diffuseLight.g + directionLight.colour.g * specularFactor;
							colorBuffer[index + 2] = mat.colour.b * diffuseLight.b + directionLight.colour.b * specularFactor;
						}
					}
				}
			}
		}
	}
}