#pragma once
#include <math.h>
#include <string>

namespace flame
{
	namespace math
	{
		struct Vector3
		{
		public:
			union { float x, r; };
			union { float y, g; };
			union { float z, b; };

			Vector3()
			{
				x = y = z = 0.f;
			}

			Vector3(float x, float y, float z)
			{
				this->x = x;
				this->y = y;
				this->z = z;
			}

			Vector3 operator*(float factor) const
			{
				auto vector = *this;
				vector.x *= factor;
				vector.y *= factor;
				vector.z *= factor;

				return vector;
			}

			Vector3 operator+(const Vector3 &right) const
			{
				return Vector3(x + right.x, y + right.y, z + right.z);
			}

			Vector3 operator-(const Vector3 &right) const
			{
				return Vector3(x - right.x, y - right.y, z - right.z);
			}

			bool operator==(const Vector3 &right) const
			{
				if (x != right.x)
					return false;
				if (y != right.y)
					return false;
				if (z != right.z)
					return false;

				return true;
			}

			bool operator!=(const Vector3 &right) const
			{
				return !(*this == right);
			}

			float Length() const
			{
				return std::sqrtf(x * x + y * y + z * z);
			}

			float LengthPow2() const
			{
				return x * x + y * y + z * z;
			}

			Vector3 GetNormalize() const
			{
				auto length = Length();
				return Vector3(x / length, y / length, z / length);
			}
		};

		inline float Dot(const Vector3 &left, const Vector3 &right)
		{
			return left.x * right.x + left.y * right.y + left.z * right.z;
		}

		inline Vector3 Reflect(const Vector3 &normal, const Vector3 &reflectVector)
		{
			return reflectVector - normal * 2.f * Dot(reflectVector, normal);
		}

		inline std::string ToString(const Vector3 &vector)
		{
			return "(" + std::to_string(vector.x) + ", " +
				std::to_string(vector.y) + ", " +
				std::to_string(vector.z) + ")";
		}
	}
}