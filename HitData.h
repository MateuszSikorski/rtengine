#pragma once
#include "Vector3.h"

namespace flame
{
	namespace math
	{
		class HitData
		{
		public:
			HitData(bool isHitted)
			{
				this->isHitted = isHitted;
			}

			HitData(bool isHitted, const Vector3 &hitPoint, const Vector3 &normal)
			{
				this->isHitted = isHitted;
				this->hitPoint = hitPoint;
				this->normal = normal;
			}

			bool IsHitted() { return isHitted; }
			Vector3 HitPoint() const { return hitPoint; }
			Vector3 Normal() const { return normal; }

		private:
			bool isHitted;
			Vector3 hitPoint;
			Vector3 normal;
		};
	}
}